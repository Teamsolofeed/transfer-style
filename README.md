## Neural Style Transfer ##

### Description ###
* A modified version of the paper [A Neural Algorithm of Artistic Style](https://arxiv.org/abs/1508.06576)
* Using the source code from this [website](http://www.subsubroutine.com/sub-subroutine/2016/11/12/painting-like-van-gogh-with-convolutional-neural-networks) as a reference point to compare with this modified version in term of execution time.

### Prerequisites ###
* Python 3.6 and pip.

### Requirements ###
* Install Tensorflow framework by following this [link](https://www.tensorflow.org/install/)
* Pip install packages such as: numpy, scipy.

### Run the code ###
* Open your command line and type:
	
	python Neural_Style.py <style_image> <content_image>
	
* **Style_image** will be the image with the artistic style that we want to transfer.
* **Content_image** will be the image we want to transfer artistic style to.
* The **Test.py** is the code from the website. Run the code by typing in the command line:
	
	python Test.py <style_image> <content_image>

### Results ###
* Image with the artistic style from another image.
* Execution time will be shown in the console (For comparison).

![Style image](Images/The_scream.jpg)
![Content image](Images/hong_kong.jpg)
![Combined image](Output/20180123_003727_The_scream_100.jpg)



